# Easy Auxiliary Files

Here you have three python classes for easy auxiliary files handle, as config files. They are made for TOML, YAML and JSON format files.

They are made to program on GNU/Linux systems.

### Installing

Copy the needed `.py` and just simply use them as a module:

`from auxjsonfiles import AuxJsonFiles` for JSON format  
`from auxtomlfiles import AuxTomlFiles` for TOML format  
`from auxyamlfiles import AuxYamlFiles` for YAML format

#### Dependencies

JSON files need:  
`python3 -m pip install pyxdg`  
`python3 -m pip install json`

TOML files need:  
`python3 -m pip install pyxdg`  
`python3 -m pip install toml`

YAML files need:  
`python3 -m pip install xdg`  
`python3 -m pip install yaml`

### Use

It works by creating an object for each file you want to manage in your program. The name of the program and the name of the file are necessary. It is also necessary specify if it is a config or other kind of file. Finally, we have to say if our file could be available for one or all user. Let's see an example:

We are going to create a YAML config file called "test.yaml" for a program called "test-program". Each user will have their own config file:

~~~
from auxyamlfiles import AuxYamlFiles

APP = "test-program"  
FILE = "test.yaml"  
myfile = AuxYamlFiles(APP, FILE, configfile=True)  
myfile.userauxfile()
~~~

Please, take notice of:

- The example uses the variables *APP* and *FILE* to remark they are string.
- The first line gives the program and file name and tells the class to create a config file.
- The second line specifies the file is for one user.

What happens:

- It creates our config file in "/home/user/.config/test-program/test.yaml"
- If we give `configfile=False` in the first line, it would create "/home/user/.local/share/test-program/test.yaml"
- If we type `myfile.globalauxfile(path-to-file)` it would create "/path-to-file/test-program/test.yaml". It is suppose `path-to-file` is a common directory for all user, like `/var/lib`, that's why the method name starts by "global" instead of "user"

If we want a toml or json file, it is exactly the same, but using AuxTomlFiles or AuxJsonFiles.

Once we create the object `myfile` we simply read and write on it by using the following methods, but without worrying about open and close the file or reminding how is the yaml (or toml or json) format.

There are only the following methods:

---
**writeall**: You have to load the whole file content in a dictionary or string and give it to the file. Other types will raise an exception.

If you use an string it should be json/toml/yaml formatted:

~~~
fruits = """
title: List of fruits
winter-fruits:
  acid:
  - lemon
  - orange
  sweet:
  - apple
  - banana
summer-fruits:
  acid:
  - strawberry
  sweet:
  - mango
  - papaya
"""
myfile.writeall(fruits)
~~~

Case of dictionary:

~~~
fruits = {'title': 'List of fruits', 'winter-fruits': {'acid': ['lemon', 'orange'], 'sweet': ['apple', 'banana']}, 'summer-fruits': {'acid': ['strawberry'], 'sweet': ['mango', 'papaya']}}
myfile.writeall(fruits)
~~~

---
**readall**: It returns the whole file content in a dictionary:

~~~
fruits = myfile.readall()
~~~

---
**readone**: It returns the value related to the key provided. If the key does not exist, it will raise an exception.

The key should be a list of all keys until the searched one:

~~~
sweet-summer-fruits = myfile.readone(["summer-fruits", "sweet"])
~~~

Even if it is a list of one:

~~~
print(myfile.readone(["title"]))
~~~

---
**writeone**: It writes the pair key-value provided. The key should be a list as in `readone` method. The value could be an str, int, bool or even dict.

If the key exist it change de value. It is the default behavior. But the key should exist, otherwise it will raise an exception:

~~~
myfile.writeone(["title"], "List of fruits by season")
~~~

If we want to add a new pair key-value, we have to specify `new=True`:

~~~
myfile.writeone(["autumn-fruits", "sweet"], ["fig"], new=True)
~~~

### Technical

Please, pay attention to the modules needed. All classes are made for python 3, but:

- The one for JSON is compatible from python >= 3.0
- The one for TOML is compatible from python >= 3.3
- The one for YAML is compatible from python >= 3.6

### Versions

#### 1.0.0

Initial version

### Author and license

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.


# Easy Auxiliary Files

Estas son tres clases escritas en python para manejar fácilmente archivos auxiliares, como un archivo de configuración. Están hechas para archivos con formatos TOML, YAML y JSON.

Están pensadas para programar sobre sistemas GNU/Linux.

### Instalación

Copia el `.py` que necesites y úsalo como un módulo en tu programa:

`from auxjsonfiles import AuxJsonFiles` for JSON format  
`from auxtomlfiles import AuxTomlFiles` for TOML format  
`from auxyamlfiles import AuxYamlFiles` for YAML format

#### Dependencias

Para los archivos JSON se necesita:  
`python3 -m pip install pyxdg`  
`python3 -m pip install json`

Para los archivos TOML se necesita:  
`python3 -m pip install pyxdg`  
`python3 -m pip install toml`

Para los archivos YAML se necesita:  
`python3 -m pip install xdg`  
`python3 -m pip install yaml`

### Uso

Las clases funcionan creando un objeto por cada archivo que quieres gestionar con tu programa. Es necesario indicar el nombre del programa, el nombre del archivo y si se va a tratar de un archivo de configuración o de otro tipo. Por último, también hay que indicar si el archivo es para el uso individual de cada usuario o si es común a todos. Veamos un ejemplo:

Vamos a crear un archivo de configuración YAML llamado "test.yaml" para un programa llamado "test-program". Cada usuario tendrá su propio archivo de configuración:

~~~
from auxyamlfiles import AuxYamlFiles

APP = "test-program"  
FILE = "test.yaml"  
myfile = AuxYamlFiles(APP, FILE, configfile=True)  
myfile.userauxfile()
~~~

Por favor, fíjese en:

- El ejemplo crea las variables *APP* y *FILE* para remarcar que se trata de strings.
- La primera línea indica el nombre del programa y el nombre del archivo y le indica a la clase que queremos crear un archivo de configuración.
- La segunda línea indica que cada usuario tendrá su propia configuración.

Qué ocurre:

- Se crea nuestro archivo de configuración en "/home/usuario/.config/test-program/test.yaml"
- Si indicamos `configfile=False` en la primera línea, creará un archivo de uso general en "/home/usuario/.local/share/test-program/test.yaml"
- Si escribimos `myfile.globalauxfile(ruta-al-archivo)` se crearía "/ruta-al-archivo/test-program/test.yaml". Se supone que la `ruta-al-archivo` es un directorio común para todos los usuarios, como `/var/lib`, por eso el nombre del método empieza por "global" en vez de por "user".

Si quiero un archivo toml o json, es exactamente igual, pero usando AuxTomlFiles o AuxJsonFiles

Una vez que creo el objeto `myfile` simplemente leemos y escribimos en el archivo usando los siguientes métodos, pero sin preocuparnos de abrir y cerrar el archivo ni de recordar cómo es el formato yaml (o toml o json).

Hay sólo estos métodos:

---
**writeall**: Debes cargar todo el contenido del archivo en una variable tipo dict o str y entregársela al archivo. Otros tipos de variable devuelven una excepción.

Si se usa un str, debe estar formateado como json/toml/yaml:

~~~
fruits = """
title: List of fruits
winter-fruits:
  acid:
  - lemon
  - orange
  sweet:
  - apple
  - banana
summer-fruits:
  acid:
  - strawberry
  sweet:
  - mango
  - papaya
"""
myfile.writeall(fruits)
~~~

Caso de un diccionario:

~~~
fruits = {'title': 'List of fruits', 'winter-fruits': {'acid': ['lemon', 'orange'], 'sweet': ['apple', 'banana']}, 'summer-fruits': {'acid': ['strawberry'], 'sweet': ['mango', 'papaya']}}
myfile.writeall(fruits)
~~~

---
**readall**: Devuelve todo el contenido del archivo en una variable dict:

~~~
fruits = myfile.readall()
~~~

---
**readone**: Devuelve el valor relacionado con la clave proporcionada. Si la clave no existe, devuelve una excepción.

La clave es una lista de todas las claves hasta la que buscamos:

~~~
sweet-summer-fruits = myfile.readone(["summer-fruits", "sweet"])
~~~

Incluso si se trata de una lista de uno:

~~~
print(myfile.readone(["title"]))
~~~

---
**writeone**: Escribe el par clave-valor indicado. La clave debe ser una lista, como en el método `readone`. El valor puede ser un str, un int, un bool o incluso un dict.

Si la clave existe, le cambiará el valor. Es el comportamiento por defecto. Pero la clave debe existir, de lo contrario devuelve una excepción:

~~~
myfile.writeone(["title"], "List of fruits by season")
~~~

Si queremos añadir un nuevo par clave-valor, lo indicamos con `new=True`:

~~~
myfile.writeone(["autumn-fruits", "sweet"], ["fig"], new=True)
~~~

### Tecnología

Por favor, fíjese en los módulos necesarios. Las tres están escritas en python 3, pero:

- La clase para JSON es compatible con python >= 3.0
- La clase para TOML es compatible con python >= 3.3
- La clase para YAML es compatible con python >= 3.6

### Versiones

#### 1.0.0

Versión inicial

### Autor y licencia

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.


#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
#
# Este módulo está hecho para simplificar la gestión de archivos auxiliares en
# formato TOML usados por tu software principal. Por ejemplo, si necesitas un
# archivo de configuración o un archivo donde guardar datos provisionales,
# puedes usar auxtomlfiles para simplificar la creación de los archivos y el
# proceso de lectura y escritura de datos.
#
# This module is made to manage some TOML files arround your main software
# (auxiliary TOML files). For instance, if you need a config file, an storage
# file, etc. you can use auxtomlfiles to make easier the file creating and data
# reading and writting.
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

from pathlib import Path
# El módulo toml es compatible con python>=3.3
import toml
# El módulo xdg es compatible con python>=3.6
# Pero el módulo pyxdg es compatible con python>=3.0
# Se usa pyxdg, que tiene un método BaseDirectory donde almacena
# las direcciones del .config del usuario (xdg_config_home) y del
# .local/share (xdg_data_home). Estas dos son variables tipo str
# y se usan como tales variables, pero hay que convertirlas a una
# clase Path para poder usarlas como una ruta de directorio.
from xdg.BaseDirectory import xdg_config_home, xdg_data_home

# La clase se nombra con class seguido de un nombre en CamelCase

class AuxTomlFiles:
    # El primer método de la clase debe ser __init__ seguido siempre
    # de la variable self, que es necesario para luego invocar el objeto que
    # estoy creando. Este primer método sirve para hacer la instancia
    # de la clase.
    # La instancia sólo prepara las variables para usar en el resto de
    # métodos. Es decir, al hacer la instancia no se crea ningún archivo JSON.
    # Para definir las variables, es necesario indicar el nombre de la
    # aplicación, el nombre del archivo que se creará y si se trata de un
    # archivo de configuración u otro tipo de archivo, entregando el valor:
    # configfile=True o configfile=False al crear la instancia.
    # Con esta información, cada vez que se hace la instancia, la clase crea
    # un objeto que representa el archivo que se trate. Es decir, un objeto
    # por archivo.
    # Ese archivo se guardará en un directorio de forma /appname/appfile
    # Hay dos métodos para crear el archivo, uno si el archivo se guardará
    # en el home del usuario y otro por si el archivo se quiere guardar en
    # otra ubicación, por ejemplo, en /var/lib
    # El resto de métodos son para leer y escribir datos en el archivo,
    # respetando el formato TOML.
    def __init__(self, appname, appfile, configfile):
        # Para hacer la instancia de la clase y crear el objeto, sólo hay que
        # entregar el nombre de la aplicación y el nombre del archivo auxiliar
        # que se quiere crear. Además, hay que indicar si queremos crear un
        # archivo de configuración o de datos. Eso se hace entregando la
        # propiedad "configfile" con True o False. Como resultado de la
        # instancia, se crean seis variables (propiedades):
        # - dos con los nombres del directorio y archivo.
        # - dos con las rutas en caso de ser un archivo de configuración o
        #   de datos.
        # - una booleana para indicar si es un archivo de datos o de
        #   configuración.
        # Las propiedades con el nombre toman el contenido de los parámetros
        # con los que se crea la instancia, por eso se utiliza la
        # variable self.
        self.appname = Path(appname)
        self.appfile = Path(appfile)
        # Defino dos propiedades (variables) con la info de los directorios de
        # configuración y datos del usuario.
        self.userdatadir = Path(xdg_data_home)
        self.userconfigdir = Path(xdg_config_home)
        # Determino el tipo de archivo que se trate, uno de datos o uno de
        # configuración. Lo hago a partir de la propiedad que se entrega al
        # crear la instancia de esta clase. Esta propiedad se entrega en
        # forma de parámetro, con configfile=True o configfile=False
        if configfile == True:
            self.configfile = True
        else:
            self.configfile = False
        pass

    # El primer método es para crear un archivo, bien de configuración,
    # bien de datos, pero en la zona de usuario. Es decir, si es de
    # configuración se crea en el ./config del usuario y si es de datos
    # en el ./local/share del usuario. Utiliza la propiedad relativa a
    # esos directorios del usuario y completa la ruta con el nombre de
    # la aplicación y el nombre del archivo.
    # De esta forma, la ruta completa del archivo que se está creando
    # queda en auxfile y la ruta completa del directorio donde se aloja
    # queda en directory. Con esta información, el método verifica si
    # existe el directorio y lo crea si no exisitiese y lo mismo con el
    # archivo TOML (crea un archivo vacío).
    def userauxfile(self):
        # La variable directory sólo la necesito en este método
        if self.configfile:
            directory = Path(self.userconfigdir / self.appname)
        else:
            directory = Path(self.userdatadir / self.appname)
        # Pero la propiedad auxfile la usaré en el resto de métodos
        self.auxfile = Path(directory / self.appfile)
        if not directory.exists():
            Path.mkdir(directory, parents=True)
        if not self.auxfile.exists():
            Path.touch(self.auxfile)
        return()

    # Si el archivo de configuración o datos no se quiere crear en el
    # espacio del usuario, sino en un espacio válido para todos los
    # usuarios (global), por ejemplo /var/lib, se utiliza este otro
    # método. Obviamente, hay que indicar el path al archivo. Este método
    # crea, dentro del path que se le indica, un directorio con el nombre
    # de la aplicación y un archivo con el nombre del archivo que se
    # indique. Es decir, hace lo mismo que el método anterior, pero sin
    # determinar el path. Eso sí, pierde sentido la variable para
    # indicar si es un archivo de configuración o no, eso lo sabe quien
    # está creando el archivo global, y debe preocuparse de indicar
    # bien el path en este método y el nombre al crear la instancia.
    def globalauxfile(self, globalfilepath):
        # La variable directory sólo la necesito en este método
        directory = Path(globalfilepath / self.appname)
        # Pero la propiedad auxfile la usaré en el resto de métodos
        self.auxfile = Path(directory / self.appfile)
        if not directory.exists():
            Path.mkdir(directory, parents=True)
        if not self.auxfile.exists():
            Path.touch(self.auxfile)
        return()

    # Este método toma un string o un diccionario con los valores del
    # archivo TOML, los convierte a formato de archivo TOML y los carga
    # en el archivo que se indica. Al hacer esto, pisa todo el contenido
    # previo del archivo TOML y lo sustituye por los valores que se le
    # están entregando ahora.
    # Si se entrega al método un objeto python que no sea string o dict,
    # devuelve error.
    def writeall(self, python_object):
        if type(python_object) == str:
            dictionary = toml.loads(python_object)
        elif type(python_object) == dict:
            dictionary = python_object.copy()
        else:
            raise Exception("Type not accepted, only str or dict")
        with open(self.auxfile, "w") as file:
            toml.dump(dictionary, file, encoder=None)
        return()

    # Este método carga el archivo TOML y devuelve toda la información del
    # archivo en forma de diccionario.
    def readall(self):
        with open(self.auxfile, "r") as file:
            alldatadictformat = toml.load(file)
        return(alldatadictformat)

    # Método para devolver un value de un par key-value del
    # diccionario que tiene el contenido del archivo TOML
    # Como pueden ser diccionarios anidados, recibe una lista
    # con la ruta al diccionario que se trate
    def readone(self, nestlist):
        # Cargo el contenido del archivo JSON en un diccionario
        with open(self.auxfile, "r") as file:
            dictcontent = toml.load(file)
        nestlenth = len(nestlist) - 1
        for counter, nest in enumerate(nestlist):
            try:
                if counter == nestlenth:
                    value = dictcontent[nest]
                else:
                    dictcontent = dictcontent[nest]
            except(KeyError, IndentationError, SyntaxError):
                raise KeyError("Key does not exist")
                raise IndentationError("Key does not exist")
                raise SyntaxError("Key does not exist")
        return(value)

    # Método para cambiar un value de un par key-value del
    # diccionario que tiene el contenido del archivo TOML
    # Como pueden ser diccionarios anidados, recibe una lista
    # con la ruta al diccionario que se trate.
    # Además, puede recibir el argumento opcional "new".
    # New es un argumento para indicar si el par clave-valor
    # que se entrega al método writeone es para añadir un par
    # clave-valor nuevo, que no estaba antes en el archivo TOML
    # o para modificar un par clave-valor existente. Con
    # new = False, significa que el par clave-valor no es
    # nuevo, es decir, existe y se trata de modificarlo. Si
    # new = True, el par clave-valor que se entrega es nuevo,
    # no existe en el archivo TOML, y por lo tanto hay que
    # crearlo, no modificarlo.
    # Por defecto, new es False, es decir, si no se entrega
    # nada, el método opera con new = False.
    def writeone(self, nestlist, value, new=False):
        # Cargo el contenido del archivo TOML en un diccionario
        with open(self.auxfile, "r") as file:
            dictcontent = toml.load(file)
        # Se asigna un alias al diccionario con el contenido
        # del TOML
        tempdict = dictcontent

        if new:
            # Al usar un alias, los cambios que se realicen a los datos
            # subyacentes del alias se reflejarán en el diccionario
            # original. Procedo a buscar el diccionario anidado
            # en el alias por la vía de las claves de la lista,
            # hasta la penúltima:
            for nest in nestlist[:-1]:
                tempdict = tempdict.setdefault(nest, {})
            # Y cambio el valor de la última clave:
            tempdict[nestlist[-1]] = value
            # El diccionario original se modificó por la modificación
            # realizada en el alias.

        if not new:
            # Primero hay que comprobar que existe la ruta al par
            # clave-valor que se entrega
            # Recorro todo el nestlist
            for nest in nestlist:
                # Compruebo si cada elemento de nestlist existe
                if nest in tempdict:
                    # Si exite, avanzo un paso en el anidado
                    tempdict = tempdict.setdefault(nest, {})
                # Si no existe, tengo que devolver el error.
                else:
                    raise Exception("Key does not exist")
            # Si la ruta al par clave-valor existe, hago lo mismo
            # que si new = True, ya que esa parte del código resuelve
            # tanto la creación de un par clave-valor nuevo como la
            # modificación de un par existente. Primero tengo que recargar
            # el alias
            tempdict = dictcontent
            for nest in nestlist[:-1]:
                tempdict = tempdict.setdefault(nest, {})
            tempdict[nestlist[-1]] = value

        # Una vez resuelta la modificación o creación del par clave-valor
        # lo paso al archivo.
        with open(self.auxfile, "w") as file:
            toml.dump(dictcontent, file, encoder=None)
        return()
